## Dependencies Framework

The Dependencies Framework defines relationships between values. Changes to dependencies propagate to their dependants, whose states are updated on-demand. Ideal for complex models with many interrelated variables, such as games and UI frameworks.

### Usage

This library is under active development, and not yet ready to be published as a Nuget package. To use this library in a project, it and its dependencies must be added to the same directory. They may be added as Git submodules.

The dependencies for this library are:
* https://gitlab.com/RecurseGames/Common

### Unity Compatibility

To support adding this repository to Unity Assets folders:
* Some directories (e.g., test projects) include a `~` suffix. This ensures they are omitted from Unity builds.
* Non-test projects use C# version 6.
