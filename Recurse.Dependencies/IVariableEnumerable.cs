﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Collections;
using System.Collections.Generic;

namespace Recurse.Dependencies
{
    /// <summary>
    /// An <see cref="IEnumerable"/> whose contents depend on the states
    /// of other objects.
    /// </summary>
    public interface IVariableEnumerable : IDependency, IEnumerable
    {
    }

    /// <summary>
    /// An <see cref="IEnumerable{T}"/> whose contents depend on the states
    /// of other objects.
    /// </summary>
    public interface IVariableEnumerable<T> : IVariableEnumerable, IEnumerable<T>
    {
    }
}
