﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Variables.Abstract
{
    /// <summary>
    /// An <see cref="IVariable"/> whose value is cached during
    /// validation.
    /// </summary>
    [Serializable]
    public abstract class CachedVariable : IDependentVariable
    {
        protected readonly VariableCache Cache = new VariableCache();

        public object Value
        {
            get
            {
                if (Cache.TryStartValidation())
                {
                    Cache.Value = GetValidValue();
                    Cache.EndValidation();
                }
                return Cache.Value;
            }
        }

        public IWeakCollection<IDependant> Dependants => Cache.Dependants;

        public void Invalidate() => Cache.Invalidate();

        protected abstract object GetValidValue();
    }

    /// <summary>
    /// An <see cref="IVariable{T}"/> whose value is cached during
    /// validation.
    /// </summary>
    [Serializable]
    public abstract class CachedVariable<T> : IDependentVariable<T>
    {
        protected readonly VariableCache<T> Cache = new VariableCache<T>();

        object IVariable.Value => Value;

        public T Value
        {
            get
            {
                if (Cache.TryStartValidation())
                {
                    Cache.Value = GetValidValue();
                    Cache.EndValidation();
                }
                return Cache.Value;
            }
        }
        
        public IWeakCollection<IDependant> Dependants => Cache.Dependants;

        public void Invalidate() => Cache.Invalidate();

        protected abstract T GetValidValue();
    }
}