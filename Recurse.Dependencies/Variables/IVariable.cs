﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Dependencies.Variables
{
    /// <summary>
    /// A value which may determine the state of other objects.
    /// </summary>
    public interface IVariable : IDependency
    {
        object Value { get; }
    }

    /// <summary>
    /// A value which may determine the state of other objects.
    /// </summary>
    public interface IVariable<out T> : IVariable
    {
        new T Value { get; }
    }
}
