﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Dependencies.Variables
{
    /// <summary>
    /// An <see cref="IVariable"/> which depends on other objects' states.
    /// </summary>
    public interface IDependentVariable : IVariable, IDependant
    {
    }

    /// <summary>
    /// An <see cref="IVariable"/> which depends on other objects' states.
    /// </summary>
    public interface IDependentVariable<out T> : IVariable<T>, IDependant
    {
    }
}
