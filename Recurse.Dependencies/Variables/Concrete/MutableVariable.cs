﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Variables.Concrete
{
    /// <summary>
    /// An <see cref="IVariable{T}"/> whose value is set externally.
    /// </summary>
    [Serializable]
    public class MutableVariable<T> : IVariable<T>
    {
        #region Backing Fields

        private T _value;

        private readonly DependantCollection _dependants = new DependantCollection();

        #endregion

        public MutableVariable()
        {
        }

        public MutableVariable(T value) : this()
        {
            _value = value;
        }

        public IWeakCollection<IDependant> Dependants => _dependants;

        object IVariable.Value => Value;

        public T Value
        {
            get
            {
                _dependants.TryValidate();
                return _value;
            }
            set
            {
                if (Equals(_value, value)) return;
                _value = value;
                _dependants.TryInvalidate();
            }
        }
    }
}