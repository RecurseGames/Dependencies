﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Variables.Concrete
{
    /// <summary>
    /// A variable with a value provided by a delegates, which
    /// derive it from other <see cref="IDependency"/> objects.
    /// The value and delegates are cached.
    /// </summary>
    public abstract class TransformedVariable<TValue> : IDependant, IDependency
    {
        private readonly DynamicVariable<TValue> _implementation = new DynamicVariable<TValue>();

        public IWeakCollection<IDependant> Dependants => _implementation.Dependants;

        public void Invalidate() => _implementation.Invalidate();

        public static TransformedVariable<TIn, TValue> Create<TIn>(IVariable<TIn> source)
            => new TransformedVariable<TIn, TValue>(source);

        protected TValue GetValue(Func<TValue> getValidValue) => _implementation.GetValue(getValidValue);
    }

    /// <summary>
    /// A variable with a value provided by a delegates, which
    /// derive it from other <see cref="IDependency"/> objects.
    /// The value and delegates are cached.
    /// </summary>
    [Serializable]
    public class TransformedVariable<TIn, TValue> : TransformedVariable<TValue>
    {
        private readonly IVariable<TIn> _source;

        private readonly OwnedDependencies _dependencies;

        [NonSerialized]
        private Func<TValue> _getValidValue;

        public TransformedVariable(IVariable<TIn> source)
        {
            _source = source;
            _source.Dependants.Add(this);
            _dependencies = new OwnedDependencies(this);
        }

        public TValue GetValue(Func<TIn, TValue> transformSource)
        {
            if (_getValidValue == null)
            {
                _getValidValue = () => transformSource(_source.Value);
            }
            return GetValue(_getValidValue);
        }

        public TValue GetValue(Func<TIn, IVariable<TValue>> transformSource) => GetValue(
            input => transformSource(input),
            input => transformSource(input) != null ? transformSource(input).Value : default(TValue));

        public TValue GetValue(Func<TIn, IDependency> getValueDependency, Func<TIn, TValue> getValue)
        {
            if (_getValidValue == null)
            {
                _getValidValue = () =>
                {
                    var valueDependency = getValueDependency(_source.Value);
                    _dependencies.Clear();
                    if (valueDependency != null)
                        _dependencies.Add(valueDependency);

                    return getValue(_source.Value);
                };
            }
            return GetValue(_getValidValue);
        }
    }
}