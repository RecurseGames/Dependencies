﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Variables.Abstract;

namespace Recurse.Dependencies.Variables.Concrete
{
    /// <summary>
    /// A variable with a value provided by a <see cref="Func{T}"/>.
    /// The value is cached.
    /// </summary>
    [Serializable]
    public class DynamicVariable<T> : IDependency, IDependant
    {
        #region Backing Fields

        [NonSerialized]
        private Func<T> _getValidValue;

        #endregion

        private readonly Implementation _implementation;

        private Func<T> GetValidValue
        {
            get { return _getValidValue; }
            set { _getValidValue = value; }
        }

        public IWeakCollection<IDependant> Dependants => _implementation.Dependants;

        public DynamicVariable()
        {
            _implementation = new Implementation(this);
        }

        public T GetValue(Func<T> getValidValue)
        {
            GetValidValue = getValidValue;
            return _implementation.Value;
        }

        public void Invalidate() => _implementation.Invalidate();

        [Serializable]
        private class Implementation : CachedVariable<T>
        {
            private readonly DynamicVariable<T> _owner;

            public Implementation(DynamicVariable<T> owner)
            {
                _owner = owner;
            }

            protected override T GetValidValue() => _owner.GetValidValue();
        }
    }
}