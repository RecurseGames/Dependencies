﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Common.Collections.Weak;

namespace Recurse.Dependencies.Variables.Concrete
{
    public static class FixedVariable
    {
        public static FixedVariable<T> Create<T>(T value)
        {
            return new FixedVariable<T>(value);
        }
    }

    /// <summary>
    /// An <see cref="IVariable{T}"/> whose value never changes.
    /// </summary>
    [Serializable]
    public class FixedVariable<T> : IVariable<T>
    {
        public static readonly IVariable<T> Default = new FixedVariable<T>(default(T));

        object IVariable.Value => Value;

        public T Value { get; }

        public IWeakCollection<IDependant> Dependants => EmptyingCollection<IDependant>.Instance;

        public FixedVariable(T value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return $"FixedVariable({(Value != null ? Value.ToString() : "null")})";
        }
    }
}