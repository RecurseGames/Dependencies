﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Enumerables
{
    [Serializable]
    public abstract class DependentEnumerable<T> : IDependant, IVariableEnumerable<T>
    {
        protected readonly DependencyState State = new DependencyState();

        protected readonly List<T> Backing = new List<T>();

        public IWeakCollection<IDependant> Dependants => State.Dependants;

        public void EnsureValid()
        {
            if (State.TryStartValidation())
            {
                OnValidating();
                State.EndValidation();
            }
        }

        public void Invalidate()
        {
            State.Invalidate();
        }

        public IEnumerator<T> GetEnumerator()
        {
            EnsureValid();
            foreach (var element in Backing)
            {
                EnsureValid();
                yield return element;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        protected virtual void OnValidating()
        {

        }
    }
}