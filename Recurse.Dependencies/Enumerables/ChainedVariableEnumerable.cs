﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;
using Recurse.Dependencies.Variables;

namespace Recurse.Dependencies.Enumerables
{
    [Serializable]
    public class ChainedVariableEnumerable : IDependant, IVariableEnumerable
    {
        #region Static Constructor

        public static ChainedVariableEnumerable<T> Create<T>(IVariableEnumerable<IVariable<T>> source)
        {
            return new ChainedVariableEnumerable<T>(source);
        }

        #endregion

        private readonly DependencyState _state = new DependencyState();

        private readonly IVariableEnumerable<IVariable> _source;

        private readonly OwnedDependencies _dependencies;

        public IWeakCollection<IDependant> Dependants => _state.Dependants;

        public ChainedVariableEnumerable(IVariableEnumerable<IVariable> source)
        {
            _dependencies = new OwnedDependencies(this);
            _source = source;

            source.Dependants.Add(this);
        }

        public void Invalidate()
        {
            _state.Invalidate();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            _state.Expose();
            _dependencies.Clear();
            foreach (var element in _source)
            {
                _dependencies.Add(element);
                yield return element.Value;
            }
        }
    }

    public class ChainedVariableEnumerable<T> : IVariableEnumerable<T>, IDependant
    {
        private readonly DependencyState _state = new DependencyState();

        private readonly IVariableEnumerable<IVariable<T>> _source;

        private readonly OwnedDependencies _dependencies;

        public IWeakCollection<IDependant> Dependants => _state.Dependants;

        public ChainedVariableEnumerable(IVariableEnumerable<IVariable<T>> source)
        {
            _dependencies = new OwnedDependencies(this);
            _source = source;

            source.Dependants.Add(this);
        }

        public void Invalidate()
        {
            _state.Invalidate();
        }

        public IEnumerator<T> GetEnumerator()
        {
            _state.Expose();
            _dependencies.Clear();
            foreach (var element in _source)
            {
                _dependencies.Add(element);
                yield return element.Value;
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
