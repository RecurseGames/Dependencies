﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;

namespace Recurse.Dependencies.Enumerables
{
    [Serializable]
    public abstract class DependentCollection<T> : DependentEnumerable<T>, ICollection<T>
    {
        public int Count
        {
            get
            {
                if (State.TryStartValidation())
                {
                    OnValidating();
                    State.EndValidation();
                }
                return Backing.Count;
            }
        }

        public bool IsReadOnly => ((IList<T>) Backing).IsReadOnly;

        public virtual void Add(T item)
        {
            Backing.Add(item);
            Invalidate();
        }

        public virtual void Clear()
        {
            Backing.Clear();
            Invalidate();
        }

        public bool Contains(T item)
        {
            if (State.TryStartValidation())
            {
                OnValidating();
                State.EndValidation();
            }
            return Backing.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            if (State.TryStartValidation())
            {
                OnValidating();
                State.EndValidation();
            }
            Backing.CopyTo(array, arrayIndex);
        }

        public virtual bool Remove(T item)
        {
            if (State.TryStartValidation())
            {
                OnValidating();
                State.EndValidation();
            }
            var result = Backing.Remove(item);
            Invalidate();
            return result;
        }
    }
}