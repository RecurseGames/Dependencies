﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Enumerables
{
    [Serializable]
    public class ListVariable<T> : IList<T>, IVariableEnumerable<T>
    {
        private readonly DependencyState _state = new DependencyState();

        private readonly IList<T> _implementation = new List<T>();

        public IWeakCollection<IDependant> Dependants => _state.Dependants;

        public T this[int index]
        {
            get
            {
                _state.Expose();
                return _implementation[index];
            }
            set
            {
                if (!Equals(_implementation[index], value))
                {
                    _implementation[index] = value;
                    _state.Invalidate();
                }
            }
        }

        public int Count
        {
            get
            {
                _state.Expose();
                return _implementation.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                _state.Expose();
                return _implementation.IsReadOnly;
            }
        }

        public void Add(T item)
        {
            _implementation.Add(item);
            _state.Invalidate();
        }

        public void Clear()
        {
            _implementation.Clear();
            _state.Invalidate();
        }

        public bool Contains(T item)
        {
            _state.Expose();
            return _implementation.Contains(item);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            _state.Expose();
            _implementation.CopyTo(array, arrayIndex);
        }

        public IEnumerator<T> GetEnumerator()
        {
            _state.Expose();
            return _implementation.GetEnumerator();
        }

        public int IndexOf(T item)
        {
            _state.Expose();
            return _implementation.IndexOf(item);
        }

        public void Insert(int index, T item)
        {
            _implementation.Insert(index, item);
            _state.Invalidate();
        }

        public bool Remove(T item)
        {
            if (!_implementation.Remove(item)) return false;
            _state.Invalidate();
            return true;
        }

        public void RemoveAt(int index)
        {
            _implementation.RemoveAt(index);
            _state.Invalidate();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}