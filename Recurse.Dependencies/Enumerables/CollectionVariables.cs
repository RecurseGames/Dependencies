﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Dependencies.Variables;

namespace Recurse.Dependencies.Enumerables
{
    /// <summary>
    /// A collection of variables from the elements of a variable collection
    /// </summary>
    public static class CollectionVariables
    {
        public static DependentCollection<IVariable<TOut>> Create<TIn, TOut>(
            DependentEnumerable<TIn> source, 
            Func<TIn, IVariable<TOut>> getVariable)
        {
            return new CollectionVariables<TIn, TOut>(source, getVariable);
        }
    }

    /// <summary>
    /// A collection of variables from the elements of a variable collection
    /// </summary>
    [Serializable]
    public class CollectionVariables<TIn, TOut> : DependentCollection<IVariable<TOut>>
    {
        private readonly DependentEnumerable<TIn> _source;

        private readonly Func<TIn, IVariable<TOut>> _getVariable;

        public CollectionVariables(
            DependentEnumerable<TIn> source, 
            Func<TIn, IVariable<TOut>> transform)
        {
            _source = source;
            _getVariable = transform;

            source.Dependants.Add(this);
        }

        protected override void OnValidating()
        {
            Backing.Clear();
            foreach (var element in _source)
            {
                Backing.Add(_getVariable(element));
            }
        }
    }
}
