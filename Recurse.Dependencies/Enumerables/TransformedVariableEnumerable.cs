﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections;
using System.Collections.Generic;
using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Enumerables
{
    public static class TransformedVariableEnumerable
    {
        public static TransformedVariableEnumerable<TIn, TOut> Create<TIn, TOut>(
            IVariableEnumerable<TIn> source, Func<TIn, TOut> transform)
        {
            return new TransformedVariableEnumerable<TIn, TOut>(source, transform);
        }
    }

    [Serializable]
    public class TransformedVariableEnumerable<TIn, TOut> : IDependant, IVariableEnumerable<TOut>
    {
        private readonly DependencyState _state = new DependencyState();

        private readonly Func<TIn, TOut> _transform;

        private IVariableEnumerable<TIn> _source;

        public IWeakCollection<IDependant> Dependants => _state.Dependants;

        public TransformedVariableEnumerable(IVariableEnumerable<TIn> source, Func<TIn, TOut> transform)
        {
            if (source == null)
                throw new ArgumentNullException("source");
            if (transform == null)
                throw new ArgumentNullException("transform");

            _transform = transform;
            _source = source;

            source.Dependants.Add(this);
        }

        public void Invalidate()
        {
            _state.Invalidate();
        }

        public IEnumerator<TOut> GetEnumerator()
        {
            _state.Expose();
            foreach (var value in _source)
            {
                yield return _transform(value);
            }
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
