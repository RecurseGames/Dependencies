﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Common.Collections.Weak;

namespace Recurse.Dependencies.Parts
{
    /// <summary>
    /// Models the state of an <see cref="IDependency"/>.
    /// </summary>
    [Serializable]
    public class DependencyState : IDependant
    {
        private readonly DependantCollection _dependants = new DependantCollection();

        private Status _status;

        /// <summary>
        /// <see cref="IDependency.Dependants"/>
        /// </summary>
        public IWeakCollection<IDependant> Dependants => _dependants;

        public bool IsValid => _status == Status.Valid;

        public DependencyState()
        {
            _status = Status.Invalid;
        }

        public void Expose()
        {
            if (TryStartValidation())
                EndValidation();
        }
        
        public void Invalidate()
        {
            if (_status == Status.Validating)
            {
                throw new InvalidOperationException(
                    "Validation should not invalidate a DependantVariable's dependencies. " +
                    "This implies a cyclic dependency has occurred.");
            }
            if (_status == Status.Valid)
            {
                _status = Status.Invalid;
                _dependants.Invalidate();
            }
        }

        /// <summary>
        /// Updates the variables internal state. Called before exposing
        /// the state publicly, if the variable has been invalidated.
        /// </summary>
        public bool TryStartValidation()
        {
            if (_status == Status.Validating)
            {
                throw new InvalidOperationException(
                    "Validation of " + GetType().Name +
                    " already in progress. This error implies a cyclic dependency has " +
                    "occurred, or EndValidation was not called due to previous error or improper " +
                    "use of this class.");
            }
            if (_status != Status.Invalid)
            {
                return false;
            }
            _status = Status.Validating;
            return true;
        }

        public void EndValidation()
        {
            if (_status != Status.Validating)
            {
                throw new InvalidOperationException("Validation not in progress.");
            }
            _status = Status.Valid;
            _dependants.Validate();
        }

        private enum Status
        {
            Invalid,

            Validating,

            Valid
        }
    }
}