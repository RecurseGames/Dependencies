﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Common.Collections.Weak;
using Recurse.Common.Messaging;
using Recurse.Common.Utils;

namespace Recurse.Dependencies.Parts
{
    /// <summary>
    /// An implementation of <see cref="IDependantCollection"/> which, after invalidating
    /// dependants, will only invalidate them again once it knows the thing they depend on
    /// has been validated. This is a necessary performance improvement.
    /// </summary>
    [Serializable]
    public class DependantCollection : WeakCollection<IDependant>
    {
        public readonly static Stack<bool> DetectCycles = new Stack<bool>();

        private bool _valid;

        public override void Add(IDependant item)
        {
            if (DetectCycles.Any() && DetectCycles.Peek())
                EnsureNoCycles(item);
            base.Add(item);
        }

        public bool TryValidate()
        {
            if (_valid) return false;
            _valid = true;
            return true;
        }

        public void Validate()
        {
            if (!TryValidate())
                throw new InvalidOperationException("Dependants already validated.");
        }

        public bool TryInvalidate()
        {
            if (!_valid) return false;
            try
            {
                foreach (var dependant in this.Distinct())
                {
                    dependant.Invalidate();
                }
            }
            catch (Exception e)
            {
                throw new CallbackException(e);
            }
            finally
            {
                _valid = false;
            }
            return true;
        }

        public void Invalidate()
        {
            if (!TryInvalidate())
                throw new InvalidOperationException("Dependants already invalidated.");
        }

        public void ForceInvalidate()
        {
            _valid = true;
            Invalidate();
        }

        private void EnsureNoCycles(IDependant dependant)
        {
            var dependantAsDependency = dependant as IDependency;
            if (dependantAsDependency == null) return;

            var cycle = Path.Find(
                    dependantAsDependency.Dependants,
                    collection => collection == this,
                    collection => collection.OfType<IDependency>().Select(v => v.Dependants));
            if (!cycle.Any()) return;

            throw new InvalidOperationException(
                "Adding dependant would create a cyclic dependency.");
        }
    }
}
