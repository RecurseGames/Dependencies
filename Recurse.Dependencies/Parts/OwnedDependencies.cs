﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using Recurse.Common.Collections;

namespace Recurse.Dependencies.Parts
{
    /// <summary>
    /// Stores dependencies which an <see cref="IDependant"/> has.
    /// Dependencies may be added more than once.
    /// </summary>
    [Serializable]
    public class OwnedDependencies : BaseCollection<IDependency>
    {
        private readonly IDependant _dependant;

        public OwnedDependencies(IDependant dependant) : base(new List<IDependency>())
        {
            _dependant = dependant;
        }

        public override void Add(IDependency item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            base.Add(item);
            item.Dependants.Add(_dependant);
        }

        public override bool Remove(IDependency item)
        {
            var result = base.Remove(item);
            item.Dependants.Remove(_dependant);
            return result;
        }

        public override void Clear()
        {
            var oldDependencies = this.ToList();
            base.Clear();
            oldDependencies.ForEach(dependency => dependency.Dependants.Remove(_dependant));
        }
    }
}