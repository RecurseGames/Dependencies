﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;

namespace Recurse.Dependencies.Parts
{
    /// <summary>
    /// Stores a dependency which an <see cref="IDependant"/> has.
    /// </summary>
    [Serializable]
    public class OwnedDependency<TDependency> where TDependency : IDependency
    {
        private readonly IDependant _owner;

        private TDependency _current;

        /// <summary>
        /// Gets and sets the dependency, ensuring its dependencies
        /// include the <see cref="IDependant"/>.
        /// </summary>
        public TDependency Current
        {
            get { return _current; }
            set
            {
                if (Equals(Current, value)) return;
                if (Current != null)
                    Current.Dependants.Remove(_owner);
                _current = value;

                if (Current != null)
                    Current.Dependants.Add(_owner);
            }
        }

        public OwnedDependency(IDependant owner)
        {
            _owner = owner;
        }
    }
}
