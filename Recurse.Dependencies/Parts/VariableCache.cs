﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using Recurse.Dependencies.Variables;

namespace Recurse.Dependencies.Parts
{
    /// <summary>
    /// A cache which stores (but does not validate) an <see cref="IVariable"/>'s value
    /// </summary>
    [Serializable]
    public class VariableCache : DependencyState
    {
        public object Value { get; set; }
    }

    /// <summary>
    /// A cache which stores (but does not validate) an <see cref="IVariable"/>'s value
    /// </summary>
    [Serializable]
    public class VariableCache<T> : DependencyState
    {
        public T Value { get; set; }
    }
}