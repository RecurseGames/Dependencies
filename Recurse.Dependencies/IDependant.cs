﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

namespace Recurse.Dependencies
{
    /// <summary>
    /// Interface for an object whose state may depend on other
    /// object's states.
    /// </summary>
    public interface IDependant
    {
        /// <summary>
        /// <para>
        /// Indicates the state of an object, on which this
        /// <see cref="IDependant"/>'s state depends, has changed or
        /// may have <em>been</em> changed.
        /// </para>
        /// <para>
        /// This makes the <see cref="IDependant"/>'s state undefined,
        /// until it is recalculated.
        /// </para>
        /// <para>
        /// When an <see cref="IDependant"/> is being invalidated, there
        /// may be other <see cref="IDependant"/>s which <em>should</em>
        /// be invalidated, but have not been invalidated yet. Hence,
        /// their states are undefined while this method is executing.
        /// Implementations of this MUST NOT access access/affect the states of
        /// other <see cref="IDependant"/>s, other than to invalidate them.
        /// In practise, implementations of this method should do very little
        /// (merely marking states as invalid).
        /// </para>
        void Invalidate();
    }
}
