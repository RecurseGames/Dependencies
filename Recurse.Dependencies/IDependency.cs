﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Collections.Weak;

namespace Recurse.Dependencies
{
    /// <summary>
    /// Interface for objects whose state can change, which may have
    /// dependants (i.e., other objects whose state depends on the
    /// <see cref="IDependency"/>'s state.
    /// </summary>
    public interface IDependency
    {
        /// <summary>
        /// The <see cref="IDependant"/>s whose states depend on
        /// this <see cref="IDependency"/>'s state. They may be added
        /// more than once, indicating multiple dependency relationships.
        /// </summary>
        /// <remarks>
        /// Generally, this collection should not be cleared. Most code
        /// will have no awareness of what other dependendants exist and
        /// whether they can be removed.
        /// </remarks>
        IWeakCollection<IDependant> Dependants { get; }
    }
}
