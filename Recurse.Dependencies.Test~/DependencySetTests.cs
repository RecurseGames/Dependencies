﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using NUnit.Framework;
using Recurse.Dependencies.Parts;
using Recurse.Dependencies.Test.Parts;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class DependencySetTests
    {
        private static OwnedDependencies Set;

        private static TestDependant Dependant;

        private static TestVariable Dependency;

        [SetUp]
        public static void Setup()
        {
            Dependant = new TestDependant();
            Set = new OwnedDependencies(Dependant);
            Dependency = new TestVariable();
        }

        [Test]
        public static void AddingDependencyDoesNotInvalidateDependant()
        {
            Set.Add(Dependency);

            Dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public static void RemovingDependencyDoesNotInvalidateDependant()
        {
            Set.Add(new TestVariable());
            Set.Remove(Set.Single());

            Dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public static void ClearingDependencyDoesNotInvalidateDependant()
        {
            Set.Add(Dependency);
            Set.Clear();

            Dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public static void AddedDependencyInvalidatesDependant()
        {
            Set.Add(Dependency);
            Dependency.Dependants.ForceInvalidate();

            Dependant.IsValid.ShouldBeFalse();
        }

        [Test]
        public static void RemovedDependencyDoesNotInvalidateDependant()
        {
            Set.Add(Dependency);
            Set.Remove(Dependency);
            Dependency.Dependants.ForceInvalidate();

            Dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public static void ClearedDependencyDoesNotInvalidateDependant()
        {
            Set.Add(Dependency);
            Set.Clear();
            Dependency.Dependants.ForceInvalidate();

            Dependant.IsValid.ShouldBeTrue();
        }
    }
}
