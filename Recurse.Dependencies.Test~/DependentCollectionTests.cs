﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using NUnit.Framework;
using Recurse.Dependencies.Enumerables;
using Recurse.Dependencies.Test.Parts;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class DependentCollectionTests
    {
        [Test]
        public static void ValidateBeforeInitialRead()
        {
            new Subject().Count.ShouldEqual(1);
            new Subject().Single().ShouldEqual(2);
        }

        [Test]
        public static void ValidateAfterWriteBeforeRead()
        {
            var instance = new Subject();

            instance.Clear();
            instance.Single().ShouldEqual(2);

            instance.Add(3);
            instance.Single().ShouldEqual(2);

            instance.Add(3);
            instance.Remove(3);
            instance.Single().ShouldEqual(2);
        }

        [Test]
        public static void OnlyInvalidateDependantsAfterRead()
        {
            var instance = new Subject();
            var dependant = new TestDependant();
            instance.Dependants.Add(dependant);

            instance.Add(3);
            instance.Count.ToString();
            dependant.IsValid.ShouldBeTrue();

            instance.Add(3);
            dependant.IsValid.ShouldBeFalse();
        }

        [Test]
        public static void InvalidateDependantsAfterWrite()
        {
            {
                var instance = new Subject();
                var dependant = new TestDependant();
                instance.Dependants.Add(dependant);
                instance.Count.ToString();
                instance.Add(3);
                dependant.IsValid.ShouldBeFalse();
            }
            {
                var instance = new Subject();
                var dependant = new TestDependant();
                instance.Dependants.Add(dependant);
                instance.Count.ToString();
                instance.Remove(3);
                dependant.IsValid.ShouldBeFalse();
            }
            {
                var instance = new Subject();
                var dependant = new TestDependant();
                instance.Dependants.Add(dependant);
                instance.Count.ToString();
                instance.Clear();
                dependant.IsValid.ShouldBeFalse();
            }
        }

        [Test]
        public static void ValidateBeforeRemove()
        {
            new Subject().Remove(2).ShouldEqual(true);
        }

        private class Subject : DependentCollection<int>
        {
            protected override void OnValidating()
            {
                Backing.Clear();
                Backing.Add(2);
            }
        }
    }
}
