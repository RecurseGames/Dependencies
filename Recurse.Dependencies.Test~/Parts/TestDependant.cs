﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Should;

namespace Recurse.Dependencies.Test.Parts
{
    public class TestDependant : IDependant
    {
        public bool IsValid { get; set; }

        public TestDependant()
        {
            IsValid = true;
        }

        public void Invalidate()
        {
            IsValid = false;
        }

        [Test]
        public static void ShouldBeValidInitially()
        {
            new TestDependant().IsValid.ShouldBeTrue();
        }
    }
}
