﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Common.Collections.Weak;
using Recurse.Dependencies.Parts;

namespace Recurse.Dependencies.Test.Parts
{
    public class TestVariable : TestDependant, IDependency
    {
        public readonly DependantCollection Dependants = new DependantCollection();

        IWeakCollection<IDependant> IDependency.Dependants => Dependants;
    }
}
