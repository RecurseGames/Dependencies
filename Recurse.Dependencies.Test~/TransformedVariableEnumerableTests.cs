﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using Recurse.Dependencies.Enumerables;
using Recurse.Dependencies.Test.Abstract;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class TransformedVariableEnumerableTests
    {
        public class WhenSourceListChanges : DependencyTests<TransformedVariableEnumerable<int, int>>
        {
            private ListVariable<int> _source;

            protected override TransformedVariableEnumerable<int, int> CreateSubject()
            {
                _source = new ListVariable<int> { 0 };
                return new TransformedVariableEnumerable<int, int>(_source, value => value + 1);
            }

            protected override void MutateState(TransformedVariableEnumerable<int, int> subject)
            {
                _source[0] = 10;
            }

            protected override void VerifyState(TransformedVariableEnumerable<int, int> subject, bool isMutated)
            {
                subject.Single().ShouldEqual(isMutated ? 11 : 1);
            }
        }
    }
}
