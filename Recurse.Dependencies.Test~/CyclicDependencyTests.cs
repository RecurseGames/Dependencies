﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;

namespace Recurse.Dependencies.Test
{
    public static class CyclicDependencyTests
    {
        [Test]
        public static void CanDetectTrivialCyclicDependencies()
        {
            //var a = new DependentVariable();
            //a.Dependants.Add(a);

            //var path = new Path(a).Path;
            //path.SequenceEqual(new[] { a, a }).ShouldBeTrue();
        }

        [Test]
        public static void CanDetectShortCyclicDependencies()
        {
            //var a = new DependentVariable();
            //var b = new DependentVariable();
            //a.Dependants.Add(b);
            //b.Dependants.Add(a);

            //var path = new Path(a).Path;
            //path.SequenceEqual(new[] { a, b, a }).ShouldBeTrue();
        }

        [Test]
        public static void CanDetectLongCyclicDependencies()
        {
            //var a = new DependentVariable();
            //var b = new DependentVariable();
            //var c = new DependentVariable();
            //a.Dependants.Add(b);
            //b.Dependants.Add(c);
            //c.Dependants.Add(a);

            //var path = new Path(a).Path;
            //path.SequenceEqual(new[] { a, c, b, a }).ShouldBeTrue();
        }
    }
}
