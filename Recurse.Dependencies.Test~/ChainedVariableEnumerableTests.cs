﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using Recurse.Dependencies.Enumerables;
using Recurse.Dependencies.Test.Abstract;
using Recurse.Dependencies.Variables;
using Recurse.Dependencies.Variables.Concrete;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class ChainedVariableEnumerableTests
    {
        public class WhenSourceVariableChanges : DependencyTests<ChainedVariableEnumerable<int>>
        {
            private ListVariable<IVariable<int>> _source;

            protected override ChainedVariableEnumerable<int> CreateSubject()
            {
                _source = new ListVariable<IVariable<int>> { new MutableVariable<int>(1) };
                return new ChainedVariableEnumerable<int>(_source);
            }

            protected override void MutateState(ChainedVariableEnumerable<int> subject)
            {
                ((MutableVariable<int>)_source[0]).Value = 2;
            }

            protected override void VerifyState(ChainedVariableEnumerable<int> subject, bool isMutated)
            {
                subject.Single().ShouldEqual(isMutated ? 2 : 1);
            }
        }

        public class UnobservedVariableChangesHaveNoImpact : DependencyTests<ChainedVariableEnumerable<int>>
        {
            private ListVariable<IVariable<int>> _source;

            protected override ChainedVariableEnumerable<int> CreateSubject()
            {
                _source = new ListVariable<IVariable<int>>
                {
                    new MutableVariable<int>(1),
                    new MutableVariable<int>(3)
                };
                return new ChainedVariableEnumerable<int>(_source);
            }

            protected override void MutateState(ChainedVariableEnumerable<int> subject)
            {
                ((MutableVariable<int>)_source[0]).Value = 2;
            }

            protected override void VerifyState(ChainedVariableEnumerable<int> subject, bool isMutated)
            {
                subject.First().ShouldEqual(isMutated ? 2 : 1);
                ((MutableVariable<int>)_source[1]).Value = 4;
            }
        }
    }
}
