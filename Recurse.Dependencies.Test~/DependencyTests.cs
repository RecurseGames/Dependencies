﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Dependencies.Parts;
using Recurse.Dependencies.Test.Parts;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class DependencyTests
    {
        private static OwnedDependency<IDependency> Set;

        private static TestDependant Dependant;

        private static TestVariable Dependency;

        [SetUp]
        public static void Setup()
        {
            Dependant = new TestDependant();
            Set = new OwnedDependency<IDependency>(Dependant);
            Dependency = new TestVariable();
        }

        [Test]
        public static void AddingDependencyDoesNotInvalidateDependant()
        {
            Set.Current = Dependency;

            Dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public static void RemovingDependencyDoesNotInvalidateDependant()
        {
            Set.Current = Dependency;
            Set.Current = null;

            Dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public static void AddedDependencyInvalidatesDependant()
        {
            Set.Current = Dependency;
            Dependency.Dependants.ForceInvalidate();

            Dependant.IsValid.ShouldBeFalse();
        }

        [Test]
        public static void RemovedDependencyDoesNotInvalidateDependant()
        {
            Set.Current = Dependency;
            Set.Current = null;
            Dependency.Dependants.ForceInvalidate();

            Dependant.IsValid.ShouldBeTrue();
        }
    }
}
