﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System;
using NUnit.Framework;
using Recurse.Common.Extensions.Stack;
using Recurse.Dependencies.Parts;
using Recurse.Dependencies.Test.Parts;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class DependantCollectionTests
    {
        [Test]
        public static void DependantCanHaveMultipleDependenciesOnSameVariable()
        {
            using (DependantCollection.DetectCycles.WithTop(true))
            {
                var dependant = new TestDependant();
                var variable = new TestVariable();

                variable.Dependants.Add(dependant);
                variable.Dependants.Add(dependant);

                variable.Dependants.Count.ShouldEqual(2);
            }
        }

        [Test]
        public static void VariableCannotDependOnItself()
        {
            using (DependantCollection.DetectCycles.WithTop(true))
            {
                var variable = new TestVariable();

                Assert.Throws<InvalidOperationException>(() => variable.Dependants.Add(variable));
            }
        }

        [Test]
        public static void VariableCannotDependOnItsDependants()
        {
            using (DependantCollection.DetectCycles.WithTop(true))
            {
                var a = new TestVariable();
                var b = new TestVariable();
                a.Dependants.Add(b);

                Assert.Throws<InvalidOperationException>(() => b.Dependants.Add(a));
            }
        }
    }
}
