﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Dependencies.Test.Parts;
using Should;

namespace Recurse.Dependencies.Test.Abstract
{
    public abstract class DependencyTests<TSubject> where TSubject : IDependency
    {
        protected virtual bool MutationAlsoAccessesState => false;

        protected virtual bool SubsequentMutationsDoNothing => false;

        [Test]
        public void VerifyInitialState()
        {
            var subject = CreateSubject();
            VerifyState(subject, false);
        }

        [Test]
        public void VerifyMutatedState()
        {
            var subject = CreateSubject();
            MutateState(subject);
            VerifyState(subject, true);
        }

        [Test]
        public void DependantNotInvalidatedByAccessingState()
        {
            var subject = CreateSubject();
            var dependant = new TestDependant();
            subject.Dependants.Add(dependant);

            VerifyState(subject, false);

            dependant.IsValid.ShouldBeTrue();
        }

        [Test]
        public void DependantNotInvalidatedWithoutAccesingState()
        {
            var subject = CreateSubject();
            var dependant = new TestDependant();
            subject.Dependants.Add(dependant);

            MutateState(subject);

            dependant.IsValid.ShouldEqual(!MutationAlsoAccessesState);
        }

        [Test]
        public void DependantNotInvalidatedByConsecutiveMutations()
        {
            var subject = CreateSubject();
            var dependant = new TestDependant();
            subject.Dependants.Add(dependant);

            VerifyState(subject, false);
            MutateState(subject);
            dependant.IsValid = true;
            MutateState(subject);

            dependant.IsValid.ShouldEqual(
                !MutationAlsoAccessesState ||
                SubsequentMutationsDoNothing);
        }

        [Test]
        public void DependantInvalidatedByMutatingAccessedState()
        {
            var subject = CreateSubject();
            var dependant = new TestDependant();
            subject.Dependants.Add(dependant);

            VerifyState(subject, false);
            MutateState(subject);

            dependant.IsValid.ShouldBeFalse();
        }

        protected abstract TSubject CreateSubject();

        protected abstract void MutateState(TSubject subject);

        protected abstract void VerifyState(TSubject subject, bool isMutated);
    }
}
