﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Dependencies.Variables;
using Should;

namespace Recurse.Dependencies.Test.Abstract
{
    public abstract class VariableTests<T> : DependencyTests<T> where T : IVariable
    {
        private object _mutatedState, _initialState;

        protected abstract T CreateSubject(out object initialState);

        protected abstract object GetMutatedState(T subject);

        protected sealed override T CreateSubject()
        {
            return CreateSubject(out _initialState);
        }

        protected sealed override void MutateState(T subject)
        {
            _mutatedState = GetMutatedState(subject);
        }

        protected sealed override void VerifyState(T subject, bool isMutated)
        {
            subject.Value.ShouldEqual(isMutated ? _mutatedState : _initialState);
        }
    }
}
