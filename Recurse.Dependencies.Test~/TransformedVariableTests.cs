﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using NUnit.Framework;
using Recurse.Dependencies.Test.Abstract;
using Recurse.Dependencies.Variables.Concrete;

namespace Recurse.Dependencies.Test
{
    public class TransformedVariableTests
    {
        public class WithSimpleTransform : DependencyTests<TransformedVariable<int, int>>
        {
            private MutableVariable<int> _source;

            protected override TransformedVariable<int, int> CreateSubject()
            {
                _source = new MutableVariable<int>(0);
                return TransformedVariable<int>.Create(_source);
            }

            protected override void MutateState(TransformedVariable<int, int> subject)
            {
                _source.Value = 1;
            }

            protected override void VerifyState(TransformedVariable<int, int> subject, bool isMutated)
            {
                var state = subject.GetValue(value => value + 1);
                Assert.AreEqual(_source.Value + 1, state);
            }
        }

        public abstract class WithComplexTransform : DependencyTests<TransformedVariable<MutableVariable<int>, int>>
        {
            private int _expectedState;

            private MutableVariable<MutableVariable<int>> _source;

            protected abstract int InitialState { get; }

            protected abstract int GetMutatedState(TransformedVariable<MutableVariable<int>, int> subject);

            protected abstract MutableVariable<MutableVariable<int>> CreateSource();

            protected override TransformedVariable<MutableVariable<int>, int> CreateSubject()
            {
                _source = CreateSource();
                _expectedState = InitialState;
                return TransformedVariable<int>.Create(_source);
            }

            protected override void MutateState(TransformedVariable<MutableVariable<int>, int> subject)
            {
                _expectedState = GetMutatedState(subject);
            }

            protected override void VerifyState(TransformedVariable<MutableVariable<int>, int> subject, bool isMutated)
            {
                var state = subject.GetValue(value => value);
                Assert.AreEqual(_expectedState, state);
            }
            
            public class WhenClearingInnerSource : WithComplexTransform
            {
                protected override int InitialState => 1;

                protected override MutableVariable<MutableVariable<int>> CreateSource()
                    => new MutableVariable<MutableVariable<int>>(new MutableVariable<int>(1));

                protected override int GetMutatedState(TransformedVariable<MutableVariable<int>, int> subject)
                {
                    _source.Value = null;
                    return 0;
                }
            }

            public class WhenChangingInnerSource : WithComplexTransform
            {
                protected override int InitialState => 0;

                protected override MutableVariable<MutableVariable<int>> CreateSource()
                    => new MutableVariable<MutableVariable<int>>();

                protected override int GetMutatedState(TransformedVariable<MutableVariable<int>, int> subject)
                {
                    _source.Value = new MutableVariable<int>(1);
                    return 1;
                }
            }

            public class WhenChangingInnerSourceValue : WithComplexTransform
            {
                protected override int InitialState => 1;

                protected override MutableVariable<MutableVariable<int>> CreateSource()
                    => new MutableVariable<MutableVariable<int>>(new MutableVariable<int>(1));

                protected override int GetMutatedState(TransformedVariable<MutableVariable<int>, int> subject)
                {
                    _source.Value.Value = 2;
                    return 2;
                }
            }
        }
    }
}
