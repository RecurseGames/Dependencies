﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using System.Linq;
using Recurse.Dependencies.Enumerables;
using Recurse.Dependencies.Test.Abstract;
using Should;

namespace Recurse.Dependencies.Test
{
    public static class ListVariableTests
    {
        public class WhenAddingItem : DependencyTests<ListVariable<int>>
        {
            protected override ListVariable<int> CreateSubject()
            {
                return new ListVariable<int>();
            }

            protected override void MutateState(ListVariable<int> subject)
            {
                subject.Add(1);
            }

            protected override void VerifyState(ListVariable<int> subject, bool isMutated)
            {
                if (isMutated)
                {
                    subject.Count.ShouldEqual(1);
                    subject.Single().ShouldEqual(1);
                }
                else
                {
                    subject.Count.ShouldEqual(0);
                }
            }
        }

        public class WhenSettingItem : DependencyTests<ListVariable<int>>
        {
            protected override ListVariable<int> CreateSubject()
            {
                return new ListVariable<int> { 0 };
            }

            protected override void MutateState(ListVariable<int> subject)
            {
                subject[0] = 1;
            }

            protected override void VerifyState(ListVariable<int> subject, bool isMutated)
            {
                subject.Count.ShouldEqual(1);
                subject.Single().ShouldEqual(isMutated ? 1 : 0);
            }
        }

        public class WhenRemovingItem : DependencyTests<ListVariable<int>>
        {
            protected override ListVariable<int> CreateSubject()
            {
                return new ListVariable<int> { 1 };
            }

            protected override void MutateState(ListVariable<int> subject)
            {
                subject.Remove(1);
            }

            protected override void VerifyState(ListVariable<int> subject, bool isMutated)
            {
                if (isMutated)
                {
                    subject.Count.ShouldEqual(0);
                }
                else
                {
                    subject.Count.ShouldEqual(1);
                    subject.Single().ShouldEqual(1);
                }
            }
        }

        public class WhenClearingItems : DependencyTests<ListVariable<int>>
        {
            protected override ListVariable<int> CreateSubject()
            {
                return new ListVariable<int> { 1 };
            }

            protected override void MutateState(ListVariable<int> subject)
            {
                subject.Clear();
            }

            protected override void VerifyState(ListVariable<int> subject, bool isMutated)
            {
                if (isMutated)
                {
                    subject.Count.ShouldEqual(0);
                }
                else
                {
                    subject.Count.ShouldEqual(1);
                    subject.Single().ShouldEqual(1);
                }
            }
        }
    }
}
