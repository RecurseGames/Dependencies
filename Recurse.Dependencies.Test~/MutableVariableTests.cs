﻿/*
 * Copyright (c) 2017 Ben Lambell.
 * 
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

using Recurse.Dependencies.Test.Abstract;
using Recurse.Dependencies.Variables.Concrete;

namespace Recurse.Dependencies.Test
{
    public static class MutableVariableTests
    {
        public class WhenSettingUninitialisedValue : VariableTests<MutableVariable<object>>
        {
            protected sealed override MutableVariable<object> CreateSubject(out object initialState)
            {
                initialState = null;
                return new MutableVariable<object>();
            }

            protected override object GetMutatedState(MutableVariable<object> subject)
            {
                var mutatedState = new object();
                subject.Value = mutatedState;
                return mutatedState;
            }
        }

        public class WhenSettingInitialisedValue : VariableTests<MutableVariable<object>>
        {
            protected sealed override MutableVariable<object> CreateSubject(out object initialState)
            {
                initialState = new object();
                return new MutableVariable<object>(initialState);
            }

            protected override object GetMutatedState(MutableVariable<object> subject)
            {
                var mutatedState = new object();
                subject.Value = mutatedState;
                return mutatedState;
            }
        }
    }
}
